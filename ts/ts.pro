TEMPLATE = aux

linux {
  first.commands = lupdate-qt5 ../visualino.pro && lrelease-qt5 ../visualino.pro
  QMAKE_EXTRA_TARGETS += first
}

win32 {
  first.commands = lupdate-qt5 $$PWD/../visualino.pro && lrelease-qt5 $$PWD/../visualino.pro
  QMAKE_EXTRA_TARGETS += first
}


QMAKE_CLEAN += *.qm
